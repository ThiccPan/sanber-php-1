@extends('adminlte/master');

@section('content')
<div class="card card-primary m-3">
  <div class="card-header">
    <h3 class="card-title">Create New Cast</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form action="/cast" method="POST">
    @csrf
    <div class="card-body">
      <div class="form-group">
        <label for="nama">nama</label>
        <input type="text" maxlength="45" class="form-control" id="nama" name="nama" placeholder="masukkan nama">
        @error('nama')
          <div class="alert alert-danger">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" min="0" max="999" class="form-control" name="umur" id="umur" placeholder="masukkan umur">
        @error('umur')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" rows="3" id="bio" name="bio" placeholder="masukkan bio"></textarea>
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Create Cast</button>
    </div>
  </form>
</div>  
@endsection