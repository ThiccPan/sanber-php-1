<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h1>Buat Account Baru!</h1>
  <h2>Sign Up Form</h2>
  <form action="welcome" method="post">
    @csrf
    <label for="firstname">First name: </label>
    <br><br>
    <input type="text" name="firstname">
    <br><br>
    <label for="lastname">Last name: </label>
    <br><br>
    <input type="text" name="lastname">
    <br><br>
    <label>Gender: </label>
    <br><br>
    <input type="radio" name="gender">
    <label for="male">Male</label>
    <br>
    <input type="radio" name="gender">
    <label for="female">Female</label>
    <br>
    <input type="radio" name="gender">
    <label for="otherG">Other</label>
    <br><br>
    <label for="nationality">Nationality: </label>
    <br><br>
    <select name="nationality">
      <option value="Indonesian">Indonesian</option>
      <option value="Singaporean">Singaporean</option>
      <option value="Malaysian">Malaysian</option>
      <option value="Australian">Australian</option>
    </select>
    <br><br>
    <label for="language">Language spoken: </label>
    <br><br>
    <input type="checkbox" name="language">
    <label for="bahasa">Bahasa</label>
    <br>
    <input type="checkbox" name="language">
    <label for="english">English</label>
    <br>
    <input type="checkbox" name="language">
    <label for="otherL">Other</label>
    <br><br>
    <label for="bio">Bio</label>
    <br><br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
    <br><br>
    <input type="submit" value="welcome">
  </form>
</body>
</html>